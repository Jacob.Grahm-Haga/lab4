package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.*;

public class ColorGrid implements IColorGrid {
  //Field Vars
  int rows;
  int cols;
  CellPosition pos = null;
  Color[][] ColorGrid;

  //Constructor
  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.ColorGrid = new Color[rows][cols];
  }

  //Methods
  @Override
  public Color get(CellPosition pos) {
    return ColorGrid[pos.row()][pos.col()];
  }

  @Override
  public void set(CellPosition pos, Color color) {
    ColorGrid[pos.row()][pos.col()] = color;
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<>();
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        CellPosition newCellPos = new CellPosition(i, j);
        cells.add(new CellColor(newCellPos, get(newCellPos)));
      }
    }
    return cells;
  }
}
