package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;
import java.util.List;
import java.awt.*;
import java.awt.geom.*;

public class GridView extends JPanel{
  //Field Vars
  IColorGrid grid;

  //Constructor
  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  //Methods
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  private void drawGrid(Graphics2D g2) {
    double margin = 30;
    Color bgColor = Color.LIGHT_GRAY;
    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - 2 * margin;
    g2.setColor(bgColor);
    Rectangle2D rect2d = new Rectangle2D.Double(margin, margin, width, height);
    g2.fill(rect2d);

    CellPositionToPixelConverter posToPixel = new CellPositionToPixelConverter(rect2d, grid, margin);
    drawCells(g2, grid, posToPixel);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection cellColorCollection, CellPositionToPixelConverter posToPixel) {
    List<CellColor> cells = cellColorCollection.getCells();
    for (CellColor cell : cells) {
      Rectangle2D rect2d = posToPixel.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();
      if (color == null) {
        color = Color.DARK_GRAY;
      }
      g2.setColor(color);
      g2.fill(rect2d);
    }
  }
}
