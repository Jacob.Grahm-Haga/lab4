package no.uib.inf101.bonus;

import no.uib.inf101.colorgrid.*;
import java.awt.*;

public class Main {
  public static void main(String[] args) {
    IColorGrid grid = new ColorGrid(2, 2);

    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 1), Color.GREEN);
    grid.set(new CellPosition(1, 0), Color.BLUE);
    grid.set(new CellPosition(1, 1), Color.BLACK);

    System.out.println(grid.getCells());
  }
}
